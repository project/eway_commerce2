<?php

namespace Drupal\eway_gate;

use Eway\Rapid;
use Eway\Rapid\Enum\ApiMethod;
use Eway\Rapid\Enum\TransactionType;
use Drupal\commerce_payment\Entity\PaymentInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\commerce_payment\Entity\PaymentMethodInterface;
use GuzzleHttp\ClientInterface;

/**
 * Class EwayTransactionManager
 *
 * @package Drupal\eway_gate
 */
class EwayTransactionManager implements TransactionManagerInterface {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  public $entityTypeManager;

  /**
   * Guzzle\Client instance.
   *
   * @var \GuzzleHttp\ClientInterface
   */
  protected $httpClient;

  /**
   * Rapid API Key.
   *
   * @var String
   */
  protected $apiKey;

  /**
   * Rapid API Password.
   *
   * @var String
   */
  protected $apiPass;

  /**
   * Pay Now Button Public API Key.
   *
   * @var String
   */
  protected $apiPNBKey;

  /**
   * An array of eCrypt API endpoints for "sandbox" and "production" use.
   *
   * @var string[]
   */
  protected $eCryptEndpoints;

  /**
   * Rapid API Environment type.
   * Either "sandbox" or "production".
   *
   * @var String
   */
  protected $apiEnv = 'sandbox';

  /**
   * Rapid API Client.
   *
   * @var \Eway\Rapid\Client | null
   */
  protected $client = NULL;

  /**
   * Constructs a new ConfigurableFieldManager object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity query factory.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, ClientInterface $http_client) {
    $this->entityTypeManager = $entity_type_manager;
    $this->httpClient = $http_client;

    $this->eCryptEndpoints = [
      'production' => 'https://api.ewaypayments.com/encrypt',
      'sandbox' => 'https://api.sandbox.ewaypayments.com/encrypt'
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function setApiKey($api_key) {
    $this->apiKey = $api_key;
  }

  /**
   * {@inheritdoc}
   */
  public function setApiPass($api_pass) {
    $this->apiPass = $api_pass;
  }

  /**
   * Set 'Pay Now Button' API Key.
   *
   * @param String $api_pnb_key
   */
  public function setPNBApiKey(String $api_pnb_key) {
    $this->apiPNBKey = $api_pnb_key;
  }

  /**
   * {@inheritdoc}
   */
  public function setApiEnv($api_env) {
    $this->apiEnv = $api_env;
  }

  public function getClient() {
    if (!$this->client && $this->apiKey && $this->apiPass && $this->apiEnv) {
      $this->client = $client = Rapid::createClient(
        $this->apiKey, $this->apiPass, $this->apiEnv
      );
    }

    return $this->client;
  }

  private function getAuthToken(PaymentMethodInterface $payment_method, array $payment_details) {
    $client = $this->getClient();

    $eCryptData = $this->encryptData([
      [
        "Name" => "Number",
        "Value" => $payment_details['number']
      ],
      [
        "Name" => "CVN",
        "Value" => $payment_details['security_code']
      ]
    ]);

    if (!$eCryptData) return FALSE;

    $customer = [
      'Country' => 'au',
      'CardDetails' => [
        'Name' => $payment_details['name'],
        'Number' => $eCryptData[0]->Value,
        'ExpiryMonth' => $payment_details['expiration']['month'],
        'ExpiryYear' => $payment_details['expiration']['year'],
        'CVN' => $eCryptData[1]->Value,
      ],
      'SaveCustomer' => TRUE
    ];

    if ($billing_profile = $payment_method->getBillingProfile()) {
      /** @var \Drupal\address\Plugin\Field\FieldType\AddressItem $billing_address */
      $billing_address = $billing_profile->get('address')->first();

      $customer['FirstName'] = $billing_address->getGivenName();
      $customer['LastName'] = $billing_address->getFamilyName();
      $customer['Street1'] = $billing_address->getAddressLine1();
      $customer['Street2'] = $billing_address->getAddressLine2();
      $customer['City'] = $billing_address->getLocality();
      $customer['PostalCode'] = $billing_address->getPostalCode();
      $customer['Country'] = $billing_address->getCountryCode();
    }

    $response = $client->createCustomer(ApiMethod::DIRECT, $customer);

    if (!$response->getErrors()) {
      return $response->Customer->TokenCustomerID;
    } else {
      foreach ($response->getErrors() as $error) {
        \Drupal::logger('eway_gate')->error('eWay Error: @message.', [
          '@message' => Rapid::getMessage($error)
        ]);
      }

      return FALSE;
    }
  }

  /**
   * @param $customerID
   *
   * @return \Eway\Rapid\Model\Customer|false|mixed
   */
  public function getCustomer($customerID) {
    $client = $this->getClient();

    $response = $client->queryCustomer($customerID);

    if (!$response->getErrors()) {
      return $response->Customers[0];
    } else {
      foreach ($response->getErrors() as $error) {
        \Drupal::logger('eway_gate')->error('eWay Error: @message.', [
          '@message' => Rapid::getMessage($error)
        ]);
      }

      return FALSE;
    }
  }

  public function createPayment(PaymentInterface $payment) {
    $client = $this->getClient();
    $payment_method = $payment->getPaymentMethod();
    $customerID = $payment_method->getRemoteId();
    $amount = $payment->getAmount();

    $transaction = [
      'Customer' => [
        'TokenCustomerID' => $customerID,
        'Email' => $payment->getOrder()->getEmail(),
      ],
      'Payment' => [
        'TotalAmount' => $amount->getNumber() * 100,
        'InvoiceReference' => $payment->getOrderId()
      ],
      'TransactionType' => TransactionType::RECURRING,
    ];

    $response = $client->createTransaction(ApiMethod::DIRECT, $transaction);

    if ($response->TransactionStatus) {
      return $response->TransactionID;
    } else {
      if ($response->getErrors()) {
        foreach ($response->getErrors() as $error) {
          \Drupal::logger('eway_gate')->error('eWay Error: @message.', [
            '@message' => Rapid::getMessage($error)
          ]);
        }
      } else {
        \Drupal::logger('eway_gate')->error('eWay Error: Payment was declined.');
      }
    }

    return FALSE;
  }

  public function updateCustomer(PaymentMethodInterface $payment_method) {
    $client = $this->getClient();
    $customerID = $payment_method->getRemoteId();

    $customer_raw = $this->getCustomer($customerID);

    if(!$customer_raw) return FALSE;

    $customer = [
      'TokenCustomerID' => $customer_raw->TokenCustomerID,
      'CardDetails' => [
        'ExpiryMonth' => $customer_raw->CardDetails->ExpiryMonth,
        'ExpiryYear' => $customer_raw->CardDetails->ExpiryYear,
      ]
    ];

    $response = $client->updateCustomer(ApiMethod::DIRECT, $customer);

    if (!$response->getErrors()) {
      return $response->Customer->TokenCustomerID;
    } else {
      foreach ($response->getErrors() as $error) {
        \Drupal::logger('eway_gate')->error('eWay Error: @message.', [
          '@message' => Rapid::getMessage($error)
        ]);
      }

      return FALSE;
    }
  }

  /**
   * {@inheritdoc}
   */
  public function preAuthorizeTransaction(PaymentMethodInterface $payment_method, array $payment_details) {
    if ($TokenCustomerID = $this->getAuthToken($payment_method, $payment_details)) {
      \Drupal::logger('eway_gate')->info('eWay: @message.', [
        '@message' => 'Authorisation Token request successful'
      ]);

      return $TokenCustomerID;
    } else {
      \Drupal::logger('eway_gate')->error('eWay: @message.', [
        '@message' => 'Authorisation Token request failed'
      ]);

      return FALSE;
    }
  }

  /**
   * {@inheritdoc}
   */
  public function paymentTransaction(PaymentInterface $payment) {
    if ($TransactionID = $this->createPayment($payment)) {
      \Drupal::logger('eway_gate')->info('eWay: @message.', [
        '@message' => 'Payment request successful'
      ]);

      return $TransactionID;
    } else {
      \Drupal::logger('eway_gate')->error('eWay: @message.', [
        '@message' => 'Payment request failed'
      ]);

      return FALSE;
    }
  }

  /**
   * {@inheritdoc}
   */
  public function updateAuthorizedTransaction(PaymentMethodInterface $payment_method) {
    if ($TokenCustomerID = $this->updateCustomer($payment_method)) {
      \Drupal::logger('eway_gate')->info('eWay: @message.', [
        '@message' => 'Authorisation Token update successful'
      ]);

      return $TokenCustomerID;
    } else {
      \Drupal::logger('eway_gate')->error('eWay: @message.', [
        '@message' => 'Authorisation Token update failed'
      ]);

      return FALSE;
    }
  }

  protected function encryptData(array $data) {
    $request = $this->httpClient->request('POST', $this->eCryptEndpoints[$this->apiEnv], [
      'headers' => [
        'Authorization' => 'Basic '. base64_encode($this->apiPNBKey. ':')
      ],
      'form_params' => [
        "Method" => "eCrypt",
        "Items" => $data
      ]
    ]);

    if ($request->getStatusCode() != 200) {
      return [];
    }

    $decoded = json_decode($request->getBody()->getContents());

    return $decoded->Items;
  }

}
