CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Maintainers

INTRODUCTION
------------
Description:
  eway_gate is a commerce payment gate that will give an ability to pay with
  eWay API.

Module project page:
  http://drupal.org/project/eway_gate

REQUIREMENTS
------------
None.

INSTALLATION
------------
Install as you would normally install a contributed Drupal module. Visit:
https://www.drupal.org/documentation/install/modules-themes/modules-8
for further information.

CONFIGURATION
-------------
1. You can create a Payment Gateway on Administration > Commerce > Configuration > Payment gateways
  page and configure API access in add form.

MAINTAINERS
-----------
eugene_jk - http://drupal.org/user/3668168
